import { Component, ViewChild } from '@angular/core';
import {Nav,NavController, Platform} from 'ionic-angular';//
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import {LoginPage} from "../pages/login/login";
import {PageSemanaPage} from "../pages/page-semana/page-semana";
import {PageCreditoPage} from "../pages/page-credito/page-credito";
import {IndexPage} from "../pages/index";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    //public navCtrl: NavController,
    public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    /*this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];*/

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  public pageIndex()
  {
    this.nav.push(IndexPage);
  }

  public pageDeposita()
  {
    this.nav.push(PageCreditoPage);
  }

  public pageCalendario()
  {
    this.nav.push(PageSemanaPage);
  }


}
