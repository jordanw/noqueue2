import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import {IndexPage} from "../index";
import {Validators, FormBuilder} from "@angular/forms";
import { MenuController } from 'ionic-angular';


import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import firebase from 'firebase';

import {Discente} from "../../model/discente/discente.model";

import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';

import {DiscenteListService} from "../../services/discente.services";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  cadastro: any = {

  };

  messageEmail:string;
  messagePassword:string;
  errorEmail = false;
  errorPassword = false;

  discenteTeste: Observable<any>;
  songs: AngularFireList<any>;

  aluno:any;
  discente = {
    chave: '',
    nome:'Rodrigo Graça',
    user:'rodrigo.graca',
    senha:'12345',
    senha_fila:'250530',
    matricula:'2014991500'
  };

  dados_user:any[] = [];
  dados_senha:any[] = [];
  dados_senha_fila:any[] = [];
  dados_matricula:any[] = [];

  credito = {
    matricula: '',
    numCartao:'',
    saldo:0,
    valor:0
  };
semana = {
  matricula:'',
  segunda:true,
  terca: true,
  quarta:true,
  quinta: true,
  sexta: true
};





  /*site = {
    url: 'javasampleapproach.com',
    description: 'Java technology',
  };*/

  //api:string = 'http:localhost:3306/ionic';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public db: AngularFireDatabase,
    private menu: MenuController,
    private discenteListService: DiscenteListService,
    private toast: ToastController
    )
  {

    /*let databse = firebase.database();
    // this.gotData,
    let ref = databse.ref('discente/').on('value',(data) =>{
      var scores = data.val();
      var keys= Object.keys(scores);

      for(var i = 0; i < keys.length; i++)
      {
        var k = keys[i];
        var user = scores[k].user;
        var senha = scores[k].senha;
        var senha_fila = scores[k].senha_fila;
        var matricula = scores[k].matricula;


        this.dados_user.push(user);
        this.dados_senha.push(senha);
        this.dados_senha_fila.push(senha_fila);
        this.dados_matricula.push(matricula);

      }
    });*/

    //console.log(ref);
    console.log('arrays de dados');
    console.log(this.dados_user);
    console.log(this.dados_senha);


    //this.db.list('semana/').push(this.semana);
    //this.db.list('discente/').push(this.discente);

    this.menu.swipeEnable(false);

    this.cadastro = this.formBuilder.group({
      nome:['', Validators.required],
      senha:['', Validators.required],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  login()
  {
    //console.log('dados do discente: ');

    let{nome, senha} = this.cadastro.controls;
    console.log('O usuario informado foi');
    console.log(nome.value);
    console.log('a senha informada foi');
    console.log(senha.value);

    let databse = firebase.database();
    // this.gotData,
    let ref = databse.ref('discente/').on('value',(data) =>{
      var scores = data.val();
      var keys= Object.keys(scores);

      for(var i = 0; i < keys.length; i++)
      {
        var k = keys[i];
        if(nome.value === scores[k].user && senha.value === scores[k].senha)
        {

          return this.navCtrl.push(IndexPage, {
            chave :this.discente.chave = keys[i],
            user:this.discente.user = scores[k].user,
            senha: this.discente.senha = scores[k].senha,
            senha_fila: this.discente.senha_fila = scores[k].senha_fila,
            nome: this.discente.nome = scores[k].nome,
            matricula: this.discente.matricula = scores[k].matricula
          });
        }else {
          this.toast.create({message:'O usuário não encontra-se cadastrado', duration: 3000}).present();
          return false;
        }
        //var user = scores[k].user;
        //var senha = scores[k].senha;
        //var senha_fila = scores[k].senha_fila;
        //var matricula = scores[k].matricula;


        //this.dados_user.push(user);
        //this.dados_senha.push(senha);
        //this.dados_senha_fila.push(senha_fila);
        //this.dados_matricula.push(matricula);

      }
    });




    /*for(var i = 0; i < this.dados_user.length; i++)
    {
      if(nome.value === this.dados_user[i] && senha.value === this.dados_senha[i])
      {
        return this.navCtrl.push(IndexPage, {aluno: this.discente.nome});
      }


      this.toast.create({message:'O usuário não encontra-se cadastrado', duration: 3000}).present();
      return false;
    }*/

  }

    getDataFromFirebase()
    {
      this.db.list('discente/').valueChanges().subscribe(
        data => console.log(data)
      );
    }


  /*login()
  {
    let{nome, senha} = this.cadastro.controls;

    if(!this.cadastro.valid)
    {
      if(!nome.valid)
      {
        console.log(nome.valid);
        this.errorEmail = true;
        this.messageEmail = "Usuário inválido";
      }else{
        this.messageEmail = "teste";
      }



      if(!senha.valid)
      {
        this.errorPassword= true;
        this.messagePassword = "A senha é obrigatória";
      }else{
        this.messagePassword = "teste";
      }
    }else{
      //alert('Login realizado');
      this.navCtrl.push(IndexPage);
    }

  }*/

}
