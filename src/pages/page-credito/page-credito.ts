import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import {PageSemanaPage} from "../page-semana/page-semana";
import {LoginPage} from "../login/login";
import firebase from "firebase";
import {IndexPage} from "../index";

import {DiscenteListService} from "../../services/discente.services";
import {Validators, FormBuilder} from "@angular/forms";

import {ChangeDetectionStrategy } from '@angular/core'


@IonicPage()
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'page-page-credito',
  templateUrl: 'page-credito.html',
})
export class PageCreditoPage {
  aluno = {
    chave:'',
    nome:'',
    user:'',
    senha:'',
    senha_fila:'',
    matricula:''
  };

  creditos = {
    chave:'',
    matricula:'',
    numCartao:'',
    saldo:0,
    valor:0
  };

  credito:any ={};


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private discenteProvider: DiscenteListService,
    private toast: ToastController,
    private formBuilder:FormBuilder)
  {

    this.aluno.chave = this.navParams.get('_chave');
    this.aluno.senha = this.navParams.get('_senha');
    this.aluno.user = this.navParams.get('_user');
    this.aluno.senha_fila = this.navParams.get('_senha_fila');
    this.aluno.nome = this.navParams.get('_nome');
    this.aluno.matricula = this.navParams.get('_matricula');

    this.credito = this.formBuilder.group({
      novoSaldo:['', Validators.required],
      novoValor:['', Validators.required],
    });

    this.menu.swipeEnable(true);
    console.log('aluno na pagina de credito:');
    console.log(this.aluno);

    //console.log('VERIFICANDO A FUNCAO dadosBancarios');
    this.dadosBancarios();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageCreditoPage');
  }

  public logout()
  {
    this.navCtrl.push(LoginPage);
  }


  public pageCalendario()
  {
    this.navCtrl.push(PageSemanaPage, {
      chave: this.aluno.chave,
      senha: this.aluno.senha,
      user: this.aluno.user,
      senha_fila: this.aluno.senha_fila,
      nome: this.aluno.nome,
      matricula: this.aluno.matricula
    });
  }

  dadosBancarios()
  {
    let databse = firebase.database();
    let ref = databse.ref('credito/').on('value',(data) => {
      var scores = data.val();
      var keys = Object.keys(scores);

      console.log('matricula de credito');
      console.log(scores);

      for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        if (this.aluno.matricula === scores[k].matricula) {
          console.log('matricula dentro do for');

          console.log(scores[k].matricula);
            this.creditos.chave = keys[i];
            this.creditos.matricula = scores[k].matricula;
            this.creditos.saldo = scores[k].saldo;
            this.creditos.valor = scores[k].valor;
            this.creditos.numCartao = scores[k].numCartao;
        }

        console.log('a chave do objeto local e');
        console.log(this.creditos.chave);

        console.log('dados bancarios');
        console.log(this.creditos);

      }
    });
    }


  salvarDadosBancarios()
  {
    let  {novoValor, novoSaldo} = this.credito.controls;
    novoValor.value = parseFloat(novoValor.value);
    novoSaldo.value = parseFloat(novoSaldo.value);

    let total = novoSaldo.value + novoValor.value;

    /*this.creditos.valor = novoValor.value;
    novoSaldo.value = this.creditos.saldo + novoValor.value;
    console.log('novo saldo');
    console.log(novoSaldo);*/

    this.creditos.saldo = total;
    console.log('novo saldo do objeto');
    console.log(this.creditos.saldo);

    if(this.discenteProvider.updateSaldo(this.creditos)){
      this.toast.create({message:'Recarga efetuada com sucesso', duration: 3000}).present();
      return
    }
    this.toast.create({message:'Falha ao recarregar, tente novamente.', duration: 3000}).present();
    return false;

  }


}
