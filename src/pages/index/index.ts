import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {PageCreditoPage} from "../page-credito/page-credito";
import {PageSemanaPage} from "../page-semana/page-semana";
import { AlertController } from 'ionic-angular';
import {Cliente} from "./cliente";
import {Fila} from "./fila";


import { Observable } from 'rxjs/Observable';
import {Discente} from "../../model/discente/discente.model";

import {DiscenteListService} from "../../services/discente.services";
import {AngularFireDatabase} from 'angularfire2/database';
import firebase from "firebase";


@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})
export class IndexPage {

  fl = new Fila();
  statusSenha:boolean;
  //noteList: Observable<Note[]>;
  noteList: Observable<any>;
  aluno = {
    chave:'',
    nome:'',
    user:'',
    senha:'',
    senha_fila:'',
    matricula:''
  };

  senha_fila;

  teste2 = {
    banco: ['a', 'b', 'c'],
  };

  data_atual = new Date();
  hora = this.data_atual.getHours() + ":" + this.data_atual.getMinutes();


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private noteListService: DiscenteListService,
    private db: AngularFireDatabase,
    private menu: MenuController,
    private toast: ToastController
    )
  {

    this.aluno.chave = this.navParams.get('chave');
    this.aluno.senha = this.navParams.get('senha');
    this.aluno.user = this.navParams.get('user');
    this.aluno.senha_fila = this.navParams.get('senha_fila');
    this.aluno.nome = this.navParams.get('nome');
    this.aluno.matricula = this.navParams.get('matricula');


    console.log('aluno logado');
    console.log(this.aluno);

    console.log('a quantidade de alunos na fila e: ');
    console.log(this.qtdFila());

    this.showMedia();
    this.fluxoFila();

    //setTimeout(this.showMedia(), 4000);

    this.menu.swipeEnable(true);

    //this.teste();

    console.log('hora atual');
    console.log(this.hora);

  }

  teste()
  {
    this.noteList = this.noteListService.getAll('discente/')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IndexPage');
    console.log('lista');
    //console.log(this.noteList);
  }



  public logout()
  {
    this.navCtrl.push(LoginPage);
  }

  public pageDeposita()
  {
    //console.log('o aluno na pagina index e');
    //console.log(this.aluno);
    this.navCtrl.push(PageCreditoPage, {
      _chave: this.aluno.chave,
      _senha: this.aluno.senha,
      _user: this.aluno.user,
      _senha_fila: this.aluno.senha_fila,
      _nome: this.aluno.nome,
      _matricula: this.aluno.matricula

  });
  }

  public pageCalendario()
  {
    this.navCtrl.push(PageSemanaPage, {
      chave: this.aluno.chave,
      senha: this.aluno.senha,
      user: this.aluno.user,
      senha_fila: this.aluno.senha_fila,
      nome: this.aluno.nome,
      matricula: this.aluno.matricula
    });
  }

  addSenha()
  {

    let fila = new Fila();
    let cliente = new Cliente();
    let date = new Date();

    fila.adic(parseInt(this.aluno.matricula));

    this.senha_fila = date.getDate() + date.getHours() + this.aluno.matricula.slice(8,10) + Math.floor(Math.random() * 10) + 1;
    console.log('a senha gerada foi: ');
    console.log(this.senha_fila);
    if(this.aluno.senha_fila === "")
    {
      this.aluno.senha_fila = this.senha_fila;
      this.noteListService.updateDiscente(this.aluno);
    }
    else {
      this.toast.create({message:'O discente já possui uma senha cadastrada', duration: 3000}).present();
    }


    console.log('dado do aluno');
    console.log(this.aluno);


  }


  public cancelaSenha() {
    let confirm = this.alertCtrl.create({
      title: 'Cancelar senha',
      message: 'Você deseja cancelar sua refeição?',
      buttons: [
        {
          text: 'CANCELAR',
          handler: () => {
            //this.statusSenha = false;
            //this.checaSenha();
            console.log('Disagree clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            this.aluno.senha_fila = "";
            this.noteListService.updateDiscente(this.aluno);
            document.getElementById('senha').innerHTML = 'Senha: ';
            this.statusSenha = true;
            console.log('Agree clicked');
          }
        }
      ]
    });
    /*if (this.statusSenha == true)
    {
      console.log('fila cancelada');
      document.getElementById('senha').innerHTML = 'Senha: ';
    }
    else {
      console.log('fila não cancelada');
    }*/
    confirm.present();
  }



  qtdFila()
  {
    let qtd:number = 0;

    let databse = firebase.database();
    // this.gotData,
    let ref = databse.ref('discente/').on('value',(data) =>{
      var scores = data.val();
      var keys= Object.keys(scores);

      for(var i = 0; i < keys.length; i++)
      {
        var k = keys[i];
        if(scores[k].senha_fila != "" || scores[k].senha_fila != null )
        {
          qtd= qtd + 1;
        }
      }

    });
    return qtd;
  }



   showMedia() {
    let avg10=[];//array com o número de alunos que passaram a cada minuto
    let media=0;//variável que armaz
    let alunosCatraca=0;

    let qtdAluno = this.qtdFila();
    console.log('quantidade de aluno');
    console.log(qtdAluno);
    console.log(typeof (qtdAluno));

    media=0;
    //alunosCatraca=Math.floor(Math.random()*20);//Alunos que passaram na catraca, seria um contador pra cada minuto.
    //console.log(alunosCatraca)

     let databse = firebase.database();
     // this.gotData,
     let ref = databse.ref('discente/').on('value',(data) =>{
       var scores = data.val();
       var keys= Object.keys(scores);

       for(var i = 0; i < keys.length; i++)
       {
         var k = keys[i];
         if(scores[k].senha_fila != "" || scores[k].senha_fila != null )
         {
           scores[k].senha_fila = "";
           media = qtdAluno /0.25;
           console.log('media');
           console.log(media);
         }
       }

     });


    /*if(avg10.length>=11){
      avg10.shift();
    }
    for (let i=0;i<avg10.length;i++){
      media+=avg10[i];
    }*/
    //media/=avg10.length;
     media = (media * 0.0166667);
     let media2 = media.toFixed(2);
    return media2;

  }

  fluxoFila()
  {
    let fluxo = (this.qtdFila() / parseFloat(this.showMedia()));
    return fluxo;
  }



}
