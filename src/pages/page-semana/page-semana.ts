import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import {PageCreditoPage} from "../page-credito/page-credito";
import {LoginPage} from "../login/login";
import {FormBuilder, Validators} from "@angular/forms";
import firebase from "firebase";

import {DiscenteListService} from "../../services/discente.services";

@IonicPage()
@Component({
  selector: 'page-page-semana',
  templateUrl: 'page-semana.html',
})
export class PageSemanaPage {

  currentDate = new Date();
  currentDate2 = Number(this.currentDate.getDate());

  almoco:any={};

  aluno = {
    chave:'',
    nome:'',
    user:'',
    senha:'',
    senha_fila:'',
    matricula:''
  };

  segunda:boolean = true;terca:boolean= true;quarta:boolean= true;quinta:boolean= true;sexta:boolean= true;

  semana:any ={
    chave:'',
    matricula:'',
    segunda:'',
    terca: '',
    quarta:'',
    quinta: '',
    sexta: ''
  };


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private formBuilder: FormBuilder,
    private provider: DiscenteListService,
    private toast: ToastController
  )
  {
    this.aluno.chave = this.navParams.get('chave');
    this.aluno.senha = this.navParams.get('senha');
    this.aluno.user = this.navParams.get('user');
    this.aluno.senha_fila = this.navParams.get('senha_fila');
    this.aluno.nome = this.navParams.get('nome');
    this.aluno.matricula = this.navParams.get('matricula');

    this.almoco = this.formBuilder.group({
      segunda:['',Validators.required],
      terca:['', Validators.required],
      quarta:['', Validators.required],
      quinta:['', Validators.required],
      sexta:['', Validators.required],
    });

    this.dadosAlmoco();


    console.log('o aluno na pagina da semana e');
    console.log(this.aluno);

    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PageSemanaPage');
  }

  diasAlmoco()
  {
    this.semana.segunda = this.segunda;
    this.semana.terca = this.terca;
    this.semana.quarta = this.quarta;
    this.semana.quinta = this.quinta;
    this.semana.sexta = this.sexta;

    if(this.provider.updateSemana(this.semana))
    {
      this.toast.create({message:'Almoço cadastrado com sucesso', duration: 3000}).present();
      return
    }

    this.toast.create({message:'Cadastro falhou, tente novamene', duration: 3000}).present();
  }


  public pageDeposita()
  {
    this.navCtrl.push(PageCreditoPage);
  }


  public logout()
  {
    this.navCtrl.push(LoginPage);
  }

  dadosAlmoco()
  {
    let databse = firebase.database();
    let ref = databse.ref('semana/').on('value',(data) => {
      var scores = data.val();
      var keys = Object.keys(scores);

      console.log('Retorno escore');
      console.log(scores);

      for (var i = 0; i < keys.length; i++) {
        var k = keys[i];
        if (this.aluno.matricula === scores[k].matricula) {
          this.semana.chave = keys[i];
          this.semana.segunda = scores[k].segunda;
          this.semana.terca = scores[k].terca;
          this.semana.quarta = scores[k].quarta;
          this.semana.quinta = scores[k].quinta;
          this.semana.sexta = scores[k].sexta;

        }

        console.log('O objeto almoco e: ');
        console.log(this.semana);


      }

      //console.log('Os dias da semana foram marcados como: ');
      //console.log(this.almoco.sexta = this.sexta);

    });
  }

}
