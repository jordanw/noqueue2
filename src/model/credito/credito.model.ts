export interface Credito
{
  key?:string,
  matricula:string,
  numCartao:string,
  saldo:number,
  valor:number,
};
