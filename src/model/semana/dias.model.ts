export interface DiasDaSemana
{
  key?:string,
  matricula:string,
  segunda:true,
  terca: true,
  quarta:true,
  quinta: true,
  sexta: true
};
