import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Discente } from '../model/discente/discente.model';

import 'rxjs/add/operator/map'

@Injectable()
export class DiscenteListService {

  private discenteListRef = this.db.list<Discente>('discente/');
  //private discenteListRef = this.db.list('discente');
  private PATHD = 'discente/';
  private PATHC = 'credito/';
  private PATHS = 'semana/';


  constructor(private db: AngularFireDatabase)
  {

  }

  getDiscenteList()
  {
    return this.discenteListRef;
  }

  getAll(path) {
    return this.db.list(path)
      .snapshotChanges()
      .map(changes => {
        console.log('Valores da response CHANGES');
        console.log(changes);
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
  }

  get(key: string) {
    return this.db.object(this.PATHD + key).snapshotChanges()
      .map(c => {
        return { key: c.key, ...c.payload.val() };
      });
  }


  addDiscente(discente: any)
  {
    this.db.list('').push({discente});
  }

  addCredito(discente:any)
  {
    this.db.list(this.PATHC).push({discente});
  }

  addSemana(discente:any)
  {
    this.db.list(this.PATHS).push({discente});
  }

  updateDiscente(discente: any)
  {
    //return this.discenteListRef.update(discente.chave, discente);
    this.db.object('discente/' + discente.chave)
      .update({ senha_fila: discente.senha_fila})

  }

  updateSaldo(credito: any)
  {
    //return this.discenteListRef.update(discente.chave, discente);
    this.db.object('credito/' + credito.chave)
      .update({ saldo: credito.saldo, valor: 0});

    return true;
  }

  updateSemana(semana: any)
  {
    //return this.discenteListRef.update(discente.chave, discente);
    this.db.object('semana/' + semana.chave)
      .update({
        segunda: semana.segunda,
        terca: semana.terca,
        quarta: semana.quarta,
        quinta: semana.quinta,
        sexta: semana.sexta,

      });

    return true;
  }

  removeDiscente(discente: any)
  {
    return this.discenteListRef.remove(discente.key);
  }
}
